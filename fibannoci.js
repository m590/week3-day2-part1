const _localFib = function (number) {
    if (number < 0) {
        return 0;
    } else if (number <= 2) {
        return 1;
    } else {
        return _localFib(number - 1) + _localFib(number - 2);
    }
}

function resolveFibannoci(number) {
    return new Promise((resolve, reject) => {
        if (number < 0) {
            reject(number + " is bad number");
        } else if (number <= 2) {
            reject(number + " is bad number");
        } else {
            resolve({number: number, result: _localFib(number)});
        }
    });
}

module.exports = resolveFibannoci;