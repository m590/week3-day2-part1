const fibannoci = require('./fibannoci');

console.log('start app');

function success(res) {
    console.log('Fibannoci of ' + res.number + ' is', res.result);
}

function error(err) {
    console.log('Fibannoci error', err);
}

fibannoci(30)
    .then(success.bind(null))
    .catch(error.bind(null));

fibannoci(-10)
    .then(success.bind(null))
    .catch(error.bind(null));

console.log('end app');
